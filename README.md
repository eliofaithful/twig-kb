![](kb-twig-logo.png)

# twig

**twig** is a git "branch control" cli driven by requirements.

**twig** is a git branch-management command-line toolset driven by requirements.

Developers can use **twig** alongside _git_ to keep the app repos on the same branch as the requirements/branch.txt file of the project they are working (known as the _home branch_), or by putting them all on a new temporary _work branch_, which you will merge back to the _home branch_ later.

## Install

```shell
git clone https://gitlab.com/eliofaithful/twig-kb ~/bin/twig
```

Add `bin` or `twig` to your shell PATH.

```shell
export PATH="~/bin:$PATH"
```

## Key

- **REQS** = the requirements/branch.txt file.
- **PRARS** = the project repo (i.e. repo of current folder) and all the repos mentioned in **REQS**.

## Intro

```shell
cd ~/repo/projects/waterboard_org
```

- Keep `projects/<myproject>/requirements/branch.txt` up to date for the branch in your active project.
- Type `twig checkout` to checkout **PRARS** onto the _home branch_ mentioned in **REQS**.
- Type `twig checkout <branch>` to checkout **PRARS** onto `<branch>`.
- Type `twig merge <branch>` to merge **PRARS** from `<branch>` onto their _home branch_ mentioned in **REQS**.
- Type `twig merge <branch> <targetbranch>` to merge **PRARS** from `<branch>` to `<targetbranch>`.

## KB Usage

Start in your project folder.

```shell
cd ~/path/to/dev/project/humanite_com
git checkout master
```

Edit `requirements/branch.txt`. You'll need paths relative to and from all the apps listed so that they can share a common root path (which twig chooses from the current project);

You should also add a row for the current project.

```
../app/api|3802-api_client-fixture
../app/base|master
../app/contact|3699-humanite-demo
../app/gallery|master
../app/gdpr|3699-humanite-demo
../app/login|3475-auth-permcheck-for-302
../app/mail|master
../app/message|master
../app/search|3699-humanite-demo
../project/humanite_com|master
```

Now you can call `twig checkout` and it will put all the apps on the given branches.

### Working on a new parent ticket

```shell
cd ~/path/to/dev/project/humanite_com
git checkout 4444-cool-new-feature
```

Edit `requirements/branch.txt` and change the _home branch_

```
../app/api|3802-api_client-fixture
../app/base|master
../app/contact|3699-humanite-demo
../app/gallery|master
../app/gdpr|3699-humanite-demo
../app/login|3475-auth-permcheck-for-302
../app/mail|master
../app/message|master
../app/search|3699-humanite-demo
../project/humanite_com|4444-cool-new-feature
```

Now any new branches you create on this "cool-new-feature" will be merged to this ticket. Commit this change.

```shell
git add .
git commit -m "starting cool-new-feature"
```

_Optional_: You could also call `twig checkout 4444-cool-new-feature` here and put all the apps on the same starting branch as **humanite_com**.

### Branch out with a new child ticket

Start work on the cool-new-feature. You probably want a new branch for any pieces of smaller work on 4444-cool-new-feature.

```shell
git add .
twig checkout 5555-starting-cool-feature-restapi
```

This will put this project and _all_ its **PRARS** on `5555-starting-cool-feature-restapi`.

Make changes on this **humanite_com**. Make changes on - for instance - **app/api** and **app/mail**.

```shell
twig commit "working in progress" 5555-cool-feature-api
```

This "adds all" and commits changes made on **humanite_com**, **app/api** and **app/mail**

### Merge back to the original branches

When you are happy with your changes.

```shell
twig merge
```

This command first "adds all" and commits on any **PRARS** with changes. Then it merges

- **humanite_com** to `4444-cool-new-feature`
- **app/api** to `3802-api_client-fixture`
- **app/mail** to `master`
- ...etc: + other unedited **PRARS** to _home branch_

_Optional_: You could also call `twig merge 5555-cool-feature-api 4444-cool-new-feature` here and put all the apps back to the same starting branch as **humanite_com**

Finally, delete `5555-starting-cool-feature-restapi` from all **PRARS**

```shell
twig snap 5555-starting-cool-feature-restapi
```

Now you can start the next child ticket. And so on.

### Finishing work parent ticket

Eventually you will have finished working on `4444-cool-new-feature` (the parent ticket) and are ready to merge your changes back.

Edit `requirements/branch.txt` and change the _home branch_ to `master` or whatever.

```
../app/api|3802-api_client-fixture
../app/base|master
../app/contact|3699-humanite-demo
../app/gallery|master
../app/gdpr|3699-humanite-demo
../app/login|3475-auth-permcheck-for-302
../app/mail|master
../app/message|master
../app/search|3699-humanite-demo
../project/humanite_com|master
```

```shell
twig commit "finishing cool-new-feature"
twig merge
```

Given that we have now indicated we want **humanite_com** back on the `master` branch; the `twig merge` command merges:

- **humanite_com** to `master`
- **app/api** to `3802-api_client-fixture`
- **app/mail** to `master`
- ...etc: + other unedited **PRARS** to _home branch_

Finally, delete `4444-cool-new-feature` humanite_com

```shell
twig snap 4444-cool-new-feature
```

However, at the risk of being impertinent, **twig** has other commands.

## Command list

### twig

`twig`

Display **twig**'s commands.

### list

`twig list`

Display a list of all the branches currently under twig's control.

### branches

`twig branches`

Run `git branches` command on **PRARS**.

### burn

`twig burn <BURNBRANCH>`

Deletes `BURNBRANCH` from remote on **PRARS**.

### checkout

`twig checkout [<CHECKOUTBRANCH>] [<MSG>]`

Checkout _home branch_ on **PRARS** having made sure it's safe. Checkout will also prompt you to `twig restore` if it finds `HOMEBRANCH-safe`

- OPT `<CHECKOUTBRANCH>`: Checkout `<CHECKOUTBRANCH>` on **PRARS** having made sure it's safe. Checkout will also prompt you to `twig restore` if it finds `CHECKOUTBRANCH-safe`
- OPT `<MSG>`: Message to use with any required commits.

### commit

`twig commit [<MSG>] [<TARGETBRANCH>]`

Add and commit current branch of **PRARS**.

- OPT `<MSG>` Message for commit.
- OPT `<TARGETBRANCH>` specify the commit branch. This will prevent **PRARS** being committed when not on the same branch.

### exit

`twig exit [<EXIT2BRANCH>] [<EXITBRANCH>]`

Exit current branch of **PRARS** and checkout _home branch_.

- OPT `<EXIT2BRANCH>` checkout to this branch instead of _home branch_.
- OPT `<EXITBRANCH>` Exit from this branch instead.

### merge

`twig merge <MERGEBRANCH> [<MERGE2BRANCH>]`

Merge **PRARS** MERGEBRANCH to _home branch_.

- OPT `<MERGE2BRANCH>` Merge to this branch instead of _home branch_.

### pull

`twig pull [<PULLBRANCH>] [<MSG>]`

Checkouts and pulls **PRARS** latest remote changes _home branch_ branchs.

- OPT `<PULLBRANCH>` Checkout and pull this branch instead of _home branch_.
- OPT `<MSG>` Message for any commits.

### push

`twig push [<PUSHBRANCH>] [<MSG>]`

Checkouts and push **PRARS** the latest commits all to _home branch_ branch.

- OPT `<PUSHBRANCH>` Checkout and push `PUSHBRANCH` branch instead of _home branch_.
- OPT `<MSG>` Message for any commits.

### restore

`twig restore <RESTOREBRANCH> [<MSG>]`

Restores **PRARS** to `RESTOREBRANCH` branch any uncommited changes put on `RESTOREBRANCH-safe`.

- OPT `<MSG>` Message for any commits.

### save

`twig save`

Save any uncommited changes on the current branch of any **PRARS** by committing them into a special purpose, recoverable branch `CURRENTBRANCH-safe`.

### snap

`twig snap <SNAPBRANCH>`

Delete the local `SNAPBRANCH` branch on any **PRARS**.

### status

`twig status`

Run `git status` command on **PRARS** .

## Testing

```bash
cd twig-kb
```

Make sure **branciao-fire** is installed as a sub repo.

- [branciao-fire](https://gitlab.com/elioangels/branciao-fire.git)

```bash
git submodule --quiet add "https://gitlab.com/elioangels/branciao-fire.git" "branciao-fire"
```

Run tests:

```bash
# Run all tests
tests/_runner
# Test individual test
tests/test_commit
```

## Contribute

- <https://gitlab.com/elioangels>

## Credits

- <https://coderwall.com/p/7smjkq/multiple-ssh-keys-for-different-accounts-on-github-or-gitlab>
- <https://stackoverflow.com/questions/37895592/how-to-use-multiple-ssh-keys-for-multiple-gitlab-accounts-with-the-same-host>
- <https://openclipart.org/detail/217197/a-simple-tree>

## License

MIT [Tim Bushell](mailto:tcbushell@gmail.com)

![](apple-touch-icon.png)
