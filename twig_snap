#!/bin/bash
# `twig snap`
set +e

source "_twig_echoempty"

CURRENTBRANCH="$(git rev-parse --abbrev-ref HEAD)"
SNAPBRANCH="${2}"
MSG="${3}"

CONFIRM="confirm"
if [[ "${4}" != "" ]]
then
  CONFIRM="${4}"
fi

if [[ "${1}" == "snap" ]]
then
  {
    if [[ ! "${SNAPBRANCH}" == "master" ]]
    then
      {
        # branch parameter is required
        if [[ ! "${SNAPBRANCH}" == "" ]]
        then
          {
            # only if not on the command branch
            if [[ "${SNAPBRANCH}" != "${CURRENTBRANCH}" ]]
            then
              {
                # if snapable
                if git show-ref --verify --quiet "refs/heads/$2"
                then
                  {
                    # get confirmation before snap
                    RESP=""
                    if [[ "${CONFIRM}" == "confirm" ]]
                    then
                      {
                        source "_twig_echodeep" "Hey! Confirm Snapping branch ${APPNAME} ${SNAPBRANCH}"
                        source "_twig_echo" "   |-- [Y] [Y]es"
                        source "_twig_echo" "   |-- [* not Y] I'll handle it manually like an adult. Thanks for letting me know."
                        read -n 1 -u 3 -p "   Y/*? " RESP
                        printf "\n"
                        source "_twig_echodeep" "You said ${RESP}"
                      }
                    else
                      RESP="Y"
                    fi

                    if [[ "${RESP}" == "Y" ]]
                    then
                      # delete the branch locally but not in remote
                      source "_twig_echo" "${LEAF_OPEN}${APPNAME}${LEAF_SHUT}.snap ${SNAPBRANCH}"
                      git branch -D "${SNAPBRANCH}"
                    fi
                  }
                else
                  {
                    source "_twig_echoerror" "SnapAborted: ${SNAPBRANCH} branch not found"
                  }
                fi #if snapable
              }
            else
              {
                source "_twig_echoerror" "SnapAborted: Checked out on ${SNAPBRANCH} branch"
                source "_twig_echo" "   |-- TryAgainAfter: \"twig checkout master\""
              }
            fi # snap if not on command branch
          }
        else
          {
            source "_twig_echoerror" "SnapAborted: <branch> parameter not supplied."
            source "_twig_echo" "   |-- Try: \"twig snap ${CURRENTBRANCH}\""
          }
        fi
      }
    else
        source "_twig_echodeep" "SnapAborted: cannot snap master branch"
    fi # no command branch
  }
fi # not our command
