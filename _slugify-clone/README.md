# Bash Command: Branchify

Branchify was a bash command that converts filenames and directories to a web friendly format.

That's when it was here:

Fork of <https://github.com/benlinton/branchify>

Now it's a bash command which converts a string to a slug and it's simply a gutted version, a fraction of the original, but it serves the purpose of turning any string in a usable branch name. We use this to blog a previous twig string. See `branciao_blog` for usage.

## Usage Help

Simply enter the branchify command without any arguments or with the -h option to view the usage help.

```
$ branchify
usage: branchify [-acdhintuv] source_file ...
   -a: remove spaces immediately adjacent to dashes
   -c: consolidate consecutive spaces into single space
   -d: replace spaces with dashes (instead of default underscores)
   -h: help
   -i: ignore case
   -t: treat existing dashes as spaces
   -u: treat existing underscores as spaces (useful with -a, -c, or -d)
```

## Usage Examples

Note, most examples below are run in verbose mode (-v) to help illustrate the results.

Verbose mode is unnecessary in real world scenarios.

### Provide escaped filenames:

```
$ branchify -v "What is going on?!"
echos: "what-is-going-on"
```

### Handle spaces adjacent to dashes:

In this example, without -a the dashes end up surrounded by underscores.

```
$ branchify -v "The Beatles - Yellow Submarine.mp3"
rename: The Beatles - Yellow Submarine.mp3 -> the_beatles_-_yellow_submarine.mp3
```

But with -a the adjacent spaces are removed.

```
$ branchify -va "The Beatles - Yellow Submarine.mp3"
rename: The Beatles - Yellow Submarine.mp3 -> the_beatles-yellow_submarine.mp3
```

The -a only removes spaces immediately adjacent to a dash, which may not be the desired effect (below three spaces on either side of the dash get converted into two underscores because of -a).

```
$ branchify -va "The Beatles   -   Yellow Submarine.mp3"
rename: The Beatles   -   Yellow Submarine.mp3 -> the_beatles__-__yellow_submarine.mp3
```

But -c consolidates spaces into a single space and then -a will remove the left over adjacent single spaces.

```
$ branchify -vac "The Beatles   -   Yellow Submarine.mp3"
rename: The Beatles - Yellow Submarine.mp3 -> the_beatles-yellow_submarine.mp3
```

### Convert existing underscores into dashes

The -u treats underscores as spaces and -d converts spaces into dashes.

```
$ branchify -vud "Spaces Dashes-And_Underscores.txt"
rename: Spaces Dashes-And_Underscores.txt -> spaces-dashes-and-underscores.txt
```

### Convert existing dashes into underscores

```
$ branchify -vt "Spaces Dashes-And_Underscores.txt"
rename: Spaces Dashes-And_Underscores.txt -> spaces_dashes_and_underscores.txt
```
