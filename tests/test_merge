#!/bin/bash
set +e
TESTNAME=$(basename "${BASH_SOURCE[0]}")
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/${TESTNAME}"
SCRIPTPATH="$( cd "$(dirname "$ABSOLUTE_PATH")" ; pwd -P )"
TWIGPATH=`echo $SCRIPTPATH |xargs dirname`
source "${TWIGPATH}/tests/_settings"
source "${BRANCIAOFIREPATH}/tests/asserts"

explain "${TESTNAME}"

before_each "${TESTFIRE1} merges to home branch but only from the branch indicated"
source "${BRANCIAOFIREPATH}/scenarios/twig_leaf_flower_leaf" "${TESTFIRE}"
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_on_branch "${TESTFIRE3}" leaf
{
  cd "${BRANCIAOFIREPATH}/${TESTFIRE2}"
  git checkout -b "change-branch"
  echo "committed change" > "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-committed.txt"
  git add .
  git commit -m "committed change"
  cd "${BRANCIAOFIREPATH}/${TESTFIRE3}"
  git checkout -b "another-branch"
  echo "committed change" > "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-committed.txt"
  git add .
  git commit -m "committed change"
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE2}" "change-branch"
assert_firebrand_is_porcelian "${TESTFIRE2}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-committed.txt"
assert_firebrand_is_on_branch "${TESTFIRE3}" "another-branch"
assert_firebrand_is_porcelian "${TESTFIRE3}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-committed.txt"
{
  cd "${BRANCIAOFIREPATH}/${TESTFIRE2}"
  source "${TWIGPATH}/twig" merge "change-branch"
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_porcelian "${TESTFIRE1}"
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_porcelian "${TESTFIRE2}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-committed.txt"
assert_firebrand_is_on_branch "${TESTFIRE3}" "another-branch"
assert_firebrand_is_porcelian "${TESTFIRE3}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-committed.txt"
