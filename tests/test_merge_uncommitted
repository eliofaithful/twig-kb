#!/bin/bash
set +e
TESTNAME=$(basename "${BASH_SOURCE[0]}")
ABSOLUTE_PATH="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/${TESTNAME}"
SCRIPTPATH="$( cd "$(dirname "$ABSOLUTE_PATH")" ; pwd -P )"
TWIGPATH=`echo $SCRIPTPATH |xargs dirname`
source "${TWIGPATH}/tests/_settings"
source "${BRANCIAOFIREPATH}/tests/asserts"

explain "${TESTNAME}"

before_each "${TESTFIRE1} merges to home branch but only from the branch indicated"
source "${BRANCIAOFIREPATH}/scenarios/twig_leaf_flower_leaf" "${TESTFIRE}"
assert_firebrand_is_on_branch "${TESTFIRE1}" twig
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_on_branch "${TESTFIRE3}" leaf
{
  cd "${BRANCIAOFIREPATH}/${TESTFIRE2}"
  source "${TWIGPATH}/twig" checkout
  source "${TWIGPATH}/twig" checkout "change-branch"
  # change NOT commit BRAND2
  echo "uncommitted change" > "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-uncommitted.txt"
  # change NOT commit BRAND3
  echo "uncommitted change" > "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-uncommitted.txt"
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE2}" "change-branch"
assert_firebrand_is_not_porcelian "${TESTFIRE2}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-uncommitted.txt"
assert_firebrand_is_on_branch "${TESTFIRE3}" "change-branch"
assert_firebrand_is_not_porcelian "${TESTFIRE3}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-uncommitted.txt"
{
  # merge BRAND2
  cd "${BRANCIAOFIREPATH}/${TESTFIRE2}"
  source "${TWIGPATH}/twig" "merge" "change-branch"
} &> /dev/null
assert_firebrand_is_on_branch "${TESTFIRE2}" leaf
assert_firebrand_is_porcelian "${TESTFIRE2}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE2}/${TESTFIRE2}-uncommitted.txt"
assert_firebrand_is_on_branch "${TESTFIRE3}" leaf
assert_firebrand_is_porcelian "${TESTFIRE3}"
assert_file_exists "${BRANCIAOFIREPATH}/${TESTFIRE3}/${TESTFIRE3}-uncommitted.txt"
